SOHO Lab Router
===============
This currently is an All In One role that creates a typical SOHO desk AP/router.
  - DHCP (kea or isc dhcp)
  - Routes (systemd-networkd, soon quagga)
  - Hostapd
  - DNS (BIND, soon core-dns)
  - Firewalld

TODO
^^^^
would like to add these
  - quagga (as mentioned above)
  - core-dns (as mentioned above)
  - pre-flight checks of data (since so much is configured via dicts/maps, we need a way of checking the structure and validity of that data before execution)
  - 

Requirements
------------
works best with systemd > 219, but it does function with Centos 7, with the caveot that not every "feature" is enabled (newer features like congestion windows and IPv6 are not set).


Role Variables
--------------

A zone must have:
serial
zone_type


Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------
Kevin Faulkner
