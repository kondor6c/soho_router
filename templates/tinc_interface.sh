#!/bin/sh
{% set order = [ "addr ", "route "] %}
{% if dest_file == "down" %}{% set order = sort(order, reverse) %}
{% else %}ip link set $INTERFACE {{ dest_file }}
{% endif %}{% for cmd in order %}
{% for subnet in subnets_v4 %}{% if subnet.type |lower == "interface" and (subnet['name'].startswith('tinc') and subnet.link_protocol |lower == "rip") %}
ip {{ cmd }} {{ subnet.address }} dev $INTERFACE
{% if subnet.routes is defined and subnet.routes |length >= 1 and order == "route" %}{% for route in subnet.routes %}
ip {{ cmd }} {{ route.destination }}/{{ route.destination_cidr }} {{ route.gateway | default("dev $INTERFACE") }}
{% endfor %}{% elif subnet.type | lower == "route" %}{# using a nested "routes" object is probably more prefered... I'm doing this any way to see how this model:
  (scope defined/relevant networks, so that anything defined within its scope is built and routes could build themselves by looking at definitions) might work#}
  {# do a sub loop here to match routes to primary interface #}
ip {{ cmd }} {{ subnet.network_v4 }}/{{ subnet.cidr }} dev $INTERFACE
{% elif subnet.link_protocol |lower == "stp" %}#https://community.scaleway.com/t/yet-another-tutorial-to-create-a-private-network/5090
brctl addif br0 eth0 eth1
ip link add name tincbr0 type bridge
ip link set tincbr0 up
ip link set $INTERFACE master tincbr0

{% endif %}{% endfor %}{% if dest_file == "down" %}
ip link set dev $INTERFACE {{ dest_file }}
{% endif %}
